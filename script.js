const searchForm = document.querySelector('#search-form');
const movies = document.querySelector('#movies');
const header = document.querySelector('#inner-text');
const posterUrl = 'https://image.tmdb.org/t/p/w500';
const trendsUrl = 'https://api.themoviedb.org/3/trending/all/day?api_key=ee0ff2134f69407c88b59586bf2e77bf&language=ru';

document.addEventListener('DOMContehtLoaded', requestApi(trendsUrl)) //загрузка трендов на главную страницу

//Поиск фильмов
function apiSearch(event) {
    event.preventDefault();
    const searchValue = document.querySelector('#searchText').value;
    const server = 'https://api.themoviedb.org/3/search/multi?api_key=ee0ff2134f69407c88b59586bf2e77bf&language=ru&query=' + searchValue;
    movies.innerHTML = `<div id='spinner'></div>`;
    header.innerHTML = '';
    if(searchValue == ''){
        movies.innerHTML = `<a id="message">Вы забыли ввести название фильма</a>`;
    }
    else requestApi(server);
}

searchForm.addEventListener('submit', apiSearch);

//Вывод на главную списка найденных фильмов
function showFilmList (info) {
    let inner = '';
    if(info.results.length === 0){
        inner = `<a id="message">По вашему запросу ничего не найдено</a>`;
    }
    info.results.forEach(function (item) {
        let nameItem = item.name || item.title;
        let mediaType = item.title ? 'movie' : 'tv';
        let img = item.poster_path ? posterUrl + item.poster_path : './img/noposter.jpeg';
        let date = item.release_date || item.first_air_date;
        if (date == undefined) return;
        let releaseDate = date.split('-', 1);
        let dataInfo = `data-id="${item.id}" data-type="${mediaType}"`;
        inner += `<div class="col-12 col-md-4 col-xl-3">
        <img id='img' src="${img}" ${dataInfo}>
        <h3 id='inner-text'>${nameItem}</h3>
        <h4 id='inner-text'>${releaseDate}</h4>
        </div>`;
    });
    movies.innerHTML = inner;

    action(); 
}

function action() {
    const list = movies.querySelectorAll('img');
    list.forEach(function(elem) {
        elem.style.cursor = 'pointer';
        elem.addEventListener('click', showFullInfo);
    })
}

//Запрос
function requestApi (server) {
    fetch(server)
        .then(checkResponseStatus)
        .then(showFilmList)
        .catch(function(err){
            movies.innerHTML = `<a id="message">Произошла ошибка</a>`;
            console.error('error: ' + err.status);
        })
}

//Проверка статуса
function checkResponseStatus (response) {
    if(response.status !== 200){
        return Promise.reject(response);
}
    return response.json();
}

//Обработчик на выбор фильма
function showFullInfo() {
    header.innerHTML = '';
    let url = '';
    if(this.dataset.type === 'movie') {
        url = 'https://api.themoviedb.org/3/movie/' + this.dataset.id + '?api_key=ee0ff2134f69407c88b59586bf2e77bf&language=ru';
    } else {
        url = 'https://api.themoviedb.org/3/tv/' + this.dataset.id + '?api_key=ee0ff2134f69407c88b59586bf2e77bf&language=ru';
    }
    fetch(url)
        .then(checkResponseStatus)
        .then(showFilmInformation)
        .catch(function(err){
            movies.innerHTML = `<a id="message">Произошла ошибка</a>`;
            console.error(err || err.status);
        })
}

function showFilmInformation(info) {
    let name = info.name || info.title;
    let type = info.title ? 'movie' : 'tv';
    let id = info.id;
    let poster = info.poster_path ? posterUrl + info.poster_path : './img/noposter.jpeg';
    movies.innerHTML = `
    <h4 class="col-12 text-center text-info">${name}</h4>
    <div class="col-4">
    <img src="${poster}">
    ${(info.homepage) ? `<p class='text-center'> <a href="${info.homepage}" target="_blank"> Официальная страница </a></p>` : ''}
    ${(info.imdb_id) ? `<p class='text-center'> <a href="https://imdb.com/title/${info.imdb_id}" target="_blank"> Страница на IMDB.com </a></p>` : ''}
    </div>
    <div class="col-8">
    <p> Рейтинг: ${info.vote_average}</p>
    <p> Премьера: ${info.release_date || info.first_air_date}</p>
    <h5 id='inner-text'> Описание </h4>
    <p>${info.overview}</p>

    <br>
    <div class="youtube"><div>
    </div>
    `
    getVideo(type, id);
}

function getVideo(type, id) {
    const youtubeUrl = `https://api.themoviedb.org/3/${type}/${id}/videos?api_key=ee0ff2134f69407c88b59586bf2e77bf&language=ru`;
    let youtube = movies.querySelector('.youtube');
    fetch(youtubeUrl)
        .then(checkResponseStatus)
        .then((info) =>{
            let videoFrame = `<h5 id='inner-text'>Видео</h5>`;
            if(info.results.length === 0) {
                return err;
            }
            info.results.forEach(function (item) {
                videoFrame += `<iframe width="560" height="315" src="https://www.youtube.com/embed/${item.key}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen id='youtube'></iframe>`;
            });
            youtube.innerHTML = videoFrame;
        })
        .catch(function(err){
            youtube.innerHTML = `<a id="message">Видео отсутствует</a>`;
            console.error(err || err.status);
        })
}



